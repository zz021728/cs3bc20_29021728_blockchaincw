﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    class Transaction
    {
        // All transaction properties
        public String hash;
        public String signature;
        public String senderAddress;
        public String receiverAddress;
        public DateTime timestamp;
        public double amount;
        public double fee;

        // Transaction constructor using transaction variables
        public Transaction(String from, String to, double amount, double fee, String privateKey)
        {
            // Assigns arguments to properties
            this.senderAddress = from;
            this.receiverAddress = to;
            this.amount = amount;
            this.fee = fee;

            this.timestamp = DateTime.Now;

            // Creates the transaction hash and the signature to go along with it
            this.hash = CreateHash();
            this.signature = Wallet.Wallet.CreateSignature(from, privateKey, this.hash);
        }

        // Hash creator method, copied from Block.cs
        public String CreateHash()
        {
            String hash = String.Empty;

            // initializes the hasher
            SHA256 hasher = SHA256Managed.Create();

            // Hashes all properties of a transaction
            String input = timestamp.ToString() + senderAddress + receiverAddress + amount.ToString() + fee.ToString();

            // Computes the hash using the hasher
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));
            // Converts the Hash from a byte array to a string
            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }

            return hash;
        }

        // ToString override to output a transaction's properties
        public override string ToString()
        {
            return "Transaction Hash: " + hash + "\n" 
                + "Digital Signature: " + signature + "\n"
                + "Timestamp: " + timestamp.ToString() + "\n"
                + "Transferred: " + amount.ToString() + " Entropycoin "
                + "     Fees: " + fee.ToString() + "\n"
                + "Sender Address: " + senderAddress + "\n"
                + "Receiver Address: " + receiverAddress + "\n";
        }
    }
}
