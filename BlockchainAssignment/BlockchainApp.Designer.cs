﻿namespace BlockchainAssignment
{
    partial class BlockchainApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.PrintBlock = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.GenerateWallet = new System.Windows.Forms.Button();
            this.publicKey = new System.Windows.Forms.TextBox();
            this.privateKey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ValidateKeys = new System.Windows.Forms.Button();
            this.CreateTransaction = new System.Windows.Forms.Button();
            this.amount = new System.Windows.Forms.TextBox();
            this.fee = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.receiverKey = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CreateBlock = new System.Windows.Forms.Button();
            this.ReadAll = new System.Windows.Forms.Button();
            this.ReadPendingTransactions = new System.Windows.Forms.Button();
            this.ValidateChain = new System.Windows.Forms.Button();
            this.CheckBalance = new System.Windows.Forms.Button();
            this.Greedy = new System.Windows.Forms.Button();
            this.Altruistic = new System.Windows.Forms.Button();
            this.Random = new System.Windows.Forms.Button();
            this.AddressBased = new System.Windows.Forms.Button();
            this.preferenceAddress = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.InfoText;
            this.richTextBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.richTextBox1.Location = new System.Drawing.Point(0, 5);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(709, 321);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // PrintBlock
            // 
            this.PrintBlock.Location = new System.Drawing.Point(12, 332);
            this.PrintBlock.Name = "PrintBlock";
            this.PrintBlock.Size = new System.Drawing.Size(75, 23);
            this.PrintBlock.TabIndex = 1;
            this.PrintBlock.Text = "Print Block";
            this.PrintBlock.UseVisualStyleBackColor = true;
            this.PrintBlock.Click += new System.EventHandler(this.PrintBlock_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(93, 334);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(44, 20);
            this.textBox1.TabIndex = 2;
            // 
            // GenerateWallet
            // 
            this.GenerateWallet.Location = new System.Drawing.Point(625, 332);
            this.GenerateWallet.Name = "GenerateWallet";
            this.GenerateWallet.Size = new System.Drawing.Size(87, 51);
            this.GenerateWallet.TabIndex = 3;
            this.GenerateWallet.Text = "Generate Wallet";
            this.GenerateWallet.UseVisualStyleBackColor = true;
            this.GenerateWallet.Click += new System.EventHandler(this.GenerateWallet_Click);
            // 
            // publicKey
            // 
            this.publicKey.Location = new System.Drawing.Point(290, 334);
            this.publicKey.Name = "publicKey";
            this.publicKey.Size = new System.Drawing.Size(328, 20);
            this.publicKey.TabIndex = 4;
            // 
            // privateKey
            // 
            this.privateKey.Location = new System.Drawing.Point(290, 363);
            this.privateKey.Name = "privateKey";
            this.privateKey.Size = new System.Drawing.Size(328, 20);
            this.privateKey.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(227, 334);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Public Key";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(227, 366);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Private Key";
            // 
            // ValidateKeys
            // 
            this.ValidateKeys.Location = new System.Drawing.Point(625, 389);
            this.ValidateKeys.Name = "ValidateKeys";
            this.ValidateKeys.Size = new System.Drawing.Size(87, 23);
            this.ValidateKeys.TabIndex = 8;
            this.ValidateKeys.Text = "Validate Keys";
            this.ValidateKeys.UseVisualStyleBackColor = true;
            this.ValidateKeys.Click += new System.EventHandler(this.ValidateKeys_Click);
            // 
            // CreateTransaction
            // 
            this.CreateTransaction.Location = new System.Drawing.Point(0, 454);
            this.CreateTransaction.Name = "CreateTransaction";
            this.CreateTransaction.Size = new System.Drawing.Size(87, 53);
            this.CreateTransaction.TabIndex = 9;
            this.CreateTransaction.Text = "Create Transaction";
            this.CreateTransaction.UseVisualStyleBackColor = true;
            this.CreateTransaction.Click += new System.EventHandler(this.CreateTransaction_Click);
            // 
            // amount
            // 
            this.amount.Location = new System.Drawing.Point(142, 454);
            this.amount.Name = "amount";
            this.amount.Size = new System.Drawing.Size(76, 20);
            this.amount.TabIndex = 10;
            // 
            // fee
            // 
            this.fee.Location = new System.Drawing.Point(142, 487);
            this.fee.Name = "fee";
            this.fee.Size = new System.Drawing.Size(76, 20);
            this.fee.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(94, 454);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Amount";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(94, 490);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Fee";
            // 
            // receiverKey
            // 
            this.receiverKey.Location = new System.Drawing.Point(316, 487);
            this.receiverKey.Name = "receiverKey";
            this.receiverKey.Size = new System.Drawing.Size(328, 20);
            this.receiverKey.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(239, 490);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Receiver Key";
            // 
            // CreateBlock
            // 
            this.CreateBlock.Location = new System.Drawing.Point(0, 513);
            this.CreateBlock.Name = "CreateBlock";
            this.CreateBlock.Size = new System.Drawing.Size(63, 50);
            this.CreateBlock.TabIndex = 16;
            this.CreateBlock.Text = "Create Block";
            this.CreateBlock.UseVisualStyleBackColor = true;
            this.CreateBlock.Click += new System.EventHandler(this.CreateBlock_Click);
            // 
            // ReadAll
            // 
            this.ReadAll.Location = new System.Drawing.Point(142, 332);
            this.ReadAll.Name = "ReadAll";
            this.ReadAll.Size = new System.Drawing.Size(75, 23);
            this.ReadAll.TabIndex = 17;
            this.ReadAll.Text = "Read All";
            this.ReadAll.UseVisualStyleBackColor = true;
            this.ReadAll.Click += new System.EventHandler(this.ReadAll_Click);
            // 
            // ReadPendingTransactions
            // 
            this.ReadPendingTransactions.Location = new System.Drawing.Point(12, 362);
            this.ReadPendingTransactions.Name = "ReadPendingTransactions";
            this.ReadPendingTransactions.Size = new System.Drawing.Size(103, 50);
            this.ReadPendingTransactions.TabIndex = 18;
            this.ReadPendingTransactions.Text = "Read Pending Transactions";
            this.ReadPendingTransactions.UseVisualStyleBackColor = true;
            this.ReadPendingTransactions.Click += new System.EventHandler(this.ReadPendingTransactions_Click);
            // 
            // ValidateChain
            // 
            this.ValidateChain.Location = new System.Drawing.Point(625, 419);
            this.ValidateChain.Name = "ValidateChain";
            this.ValidateChain.Size = new System.Drawing.Size(87, 23);
            this.ValidateChain.TabIndex = 19;
            this.ValidateChain.Text = "Validate Chain";
            this.ValidateChain.UseVisualStyleBackColor = true;
            this.ValidateChain.Click += new System.EventHandler(this.ValidateChain_Click);
            // 
            // CheckBalance
            // 
            this.CheckBalance.Location = new System.Drawing.Point(525, 389);
            this.CheckBalance.Name = "CheckBalance";
            this.CheckBalance.Size = new System.Drawing.Size(94, 23);
            this.CheckBalance.TabIndex = 20;
            this.CheckBalance.Text = "Check Balance";
            this.CheckBalance.UseVisualStyleBackColor = true;
            this.CheckBalance.Click += new System.EventHandler(this.CheckBalance_Click);
            // 
            // Greedy
            // 
            this.Greedy.Location = new System.Drawing.Point(230, 534);
            this.Greedy.Name = "Greedy";
            this.Greedy.Size = new System.Drawing.Size(75, 29);
            this.Greedy.TabIndex = 21;
            this.Greedy.Text = "Greedy";
            this.Greedy.UseVisualStyleBackColor = true;
            this.Greedy.Click += new System.EventHandler(this.Greedy_Click);
            // 
            // Altruistic
            // 
            this.Altruistic.Location = new System.Drawing.Point(69, 534);
            this.Altruistic.Name = "Altruistic";
            this.Altruistic.Size = new System.Drawing.Size(75, 29);
            this.Altruistic.TabIndex = 22;
            this.Altruistic.Text = "Altruistic";
            this.Altruistic.UseVisualStyleBackColor = true;
            this.Altruistic.Click += new System.EventHandler(this.Altruistic_Click);
            // 
            // Random
            // 
            this.Random.Location = new System.Drawing.Point(150, 534);
            this.Random.Name = "Random";
            this.Random.Size = new System.Drawing.Size(75, 29);
            this.Random.TabIndex = 23;
            this.Random.Text = "Random";
            this.Random.UseVisualStyleBackColor = true;
            this.Random.Click += new System.EventHandler(this.Random_Click);
            // 
            // AddressBased
            // 
            this.AddressBased.Location = new System.Drawing.Point(0, 569);
            this.AddressBased.Name = "AddressBased";
            this.AddressBased.Size = new System.Drawing.Size(75, 34);
            this.AddressBased.TabIndex = 24;
            this.AddressBased.Text = "Address Based";
            this.AddressBased.UseVisualStyleBackColor = true;
            this.AddressBased.Click += new System.EventHandler(this.AddressBased_Click);
            // 
            // preferenceAddress
            // 
            this.preferenceAddress.Location = new System.Drawing.Point(81, 583);
            this.preferenceAddress.Name = "preferenceAddress";
            this.preferenceAddress.Size = new System.Drawing.Size(328, 20);
            this.preferenceAddress.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(81, 569);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Preference Address";
            // 
            // BlockchainApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1229, 607);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.preferenceAddress);
            this.Controls.Add(this.AddressBased);
            this.Controls.Add(this.Random);
            this.Controls.Add(this.Altruistic);
            this.Controls.Add(this.Greedy);
            this.Controls.Add(this.CheckBalance);
            this.Controls.Add(this.ValidateChain);
            this.Controls.Add(this.ReadPendingTransactions);
            this.Controls.Add(this.ReadAll);
            this.Controls.Add(this.CreateBlock);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.receiverKey);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.fee);
            this.Controls.Add(this.amount);
            this.Controls.Add(this.CreateTransaction);
            this.Controls.Add(this.ValidateKeys);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.privateKey);
            this.Controls.Add(this.publicKey);
            this.Controls.Add(this.GenerateWallet);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.PrintBlock);
            this.Controls.Add(this.richTextBox1);
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "BlockchainApp";
            this.Text = "Blockchain App";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button PrintBlock;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button GenerateWallet;
        private System.Windows.Forms.TextBox publicKey;
        private System.Windows.Forms.TextBox privateKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ValidateKeys;
        private System.Windows.Forms.Button CreateTransaction;
        private System.Windows.Forms.TextBox amount;
        private System.Windows.Forms.TextBox fee;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox receiverKey;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button CreateBlock;
        private System.Windows.Forms.Button ReadAll;
        private System.Windows.Forms.Button ReadPendingTransactions;
        private System.Windows.Forms.Button ValidateChain;
        private System.Windows.Forms.Button CheckBalance;
        private System.Windows.Forms.Button Greedy;
        private System.Windows.Forms.Button Altruistic;
        private System.Windows.Forms.Button Random;
        private System.Windows.Forms.Button AddressBased;
        private System.Windows.Forms.TextBox preferenceAddress;
        private System.Windows.Forms.Label label6;
    }
}

