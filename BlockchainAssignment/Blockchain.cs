﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BlockchainAssignment
{
    internal class Blockchain
    {
        // Blockchains are made up of blocks, here we create a list containing the blocks.
        public List<Block> Blocks = new List<Block>();

        // Generate a new transaction pool to hold the transactions
        public List<Transaction> transactionPool = new List<Transaction>();
        int transactionsPerBlock = 5;

        // Creates the Genesis Block and adds it to the list of blocks.
        public Blockchain()
        {
            Blocks.Add(new Block());
        }

        // Get the previous block
        public Block GetLastBlock()
        {
            return Blocks[Blocks.Count - 1];
        }

        // Gets the pending transactions using the transaction pool and transactions per block
        public List<Transaction> GetPendingTransactions()
        {
            int n = Math.Min(transactionsPerBlock, transactionPool.Count());
            List<Transaction> transactions = transactionPool.GetRange(0, n);
            transactionPool.RemoveRange(0, n);

            return transactions;
        }

        public List<Transaction> GetPendingTransactions(String type)
        {
            int n = Math.Min(transactionsPerBlock, transactionPool.Count());
            List<Transaction> transactions = new List<Transaction>();

            if(type == "Greedy")
            {
                List<Transaction> sortedTransactionPool = transactionPool.OrderByDescending(t => t.fee).ToList(); // Creates a variable that has the transactions sorted by fee transactions of the pool
                
                for (int i = 0; i < n; i++)
                {
                    transactions.Add(sortedTransactionPool.ElementAt(i));// adds to the transactions list the transaction
                    transactionPool.Remove(sortedTransactionPool.ElementAt(i)); // removes from the pool the transaction
                }

            } 
            else if (type == "Altruistic")
            {
                List<Transaction> sortedTransactionPool = transactionPool.OrderBy(t => t.timestamp).ToList(); // Creates a variable that has the transactions sorted by timestamp of transactions of the pool

                for (int i = 0; i < n; i++)
                {
                    transactions.Add(sortedTransactionPool.ElementAt(i));// adds to the transactions list the transaction
                    transactionPool.Remove(sortedTransactionPool.ElementAt(i)); // removes from the pool the transaction
                }
            }
            else if (type == "Random")
            {
                int index; // Create an index for which item to add
                var rand = new Random(); // Creates the random variable method

                // Iterates until it reaches the number of transactions per block
                for (int i = 0; i < n; i++)
                {
                    index = rand.Next(transactionPool.Count); // Creates the next random index from the transactionPool
                    transactions.Add(transactionPool.ElementAt(index)); // Adds from the pool to the list of transactions
                    transactionPool.RemoveAt(index); // Removes from the pool to not be repeated
                }
            }
            else
            {
                transactions = transactionPool.GetRange(0, n);
                transactionPool.RemoveRange(0, n);
            }

            return transactions;

        }

        public List<Transaction> GetPendingTransactions(String type, String preferenceAddress)
        {
            int n = Math.Min(transactionsPerBlock, transactionPool.Count());
            List<Transaction> transactions = new List<Transaction>();

            List<Transaction> sortedTransactionPool = transactionPool.Where(t => t.senderAddress == preferenceAddress).ToList(); // Creates a variable that has the transactions of the pool sorted by preferred address

            if (type == "AddressBased")
            {
                for (int i = 0; i < sortedTransactionPool.Count && i < n; i++)
                {
                    transactions.Add(sortedTransactionPool.ElementAt(i));// adds to the transactions list the transaction
                    transactionPool.Remove(sortedTransactionPool.ElementAt(i)); // removes from the pool the transaction
                }

                if (transactions.Count < n)
                {
                    sortedTransactionPool = transactionPool.OrderByDescending(t => t.fee).ToList(); // Same as greedy option for sorting transactions in pool
                    for (int i = 0; i < n-transactions.Count; i++)
                    {
                        transactions.Add(sortedTransactionPool.ElementAt(i));// adds to the transactions list the transaction
                        transactionPool.Remove(sortedTransactionPool.ElementAt(i)); // removes from the pool the transaction
                    }
                }
            }
            else
            {
                transactions = transactionPool.GetRange(0, n);
                transactionPool.RemoveRange(0, n);
            }
            return transactions;
        }


        // Calls the ToString() override to output the Block components as a string
        public String GetBlockAsString(int index)
        {
            return Blocks[index].ToString();
        }

        // ToString overide to output the information of each block
        public override string ToString()
        {
            String output = String.Empty;
            Blocks.ForEach(b => output += (b.ToString() + "\n"));
            return output;
        }

        // Method to get the balance of an address
        public double GetBalance(String address)
        {
            double balance = 0.0;
            // Iterates through each block's transactions
            foreach (Block b in Blocks)
            {
                foreach(Transaction t in b.transactionList)
                {
                    if (t.receiverAddress.Equals(address))
                    {
                        balance += t.amount;
                    }
                    if(t.senderAddress.Equals(address))
                    {
                        balance -= (t.amount + t.fee);
                    }
                }
            }
            // Iterates through still pending transactions
            foreach (Transaction t in transactionPool)
            {
                if (t.receiverAddress.Equals(address))
                {
                    balance += t.amount;
                }
                if (t.senderAddress.Equals(address))
                {
                    balance -= (t.amount + t.fee);
                }
            }
            return balance;
        }

        // Method to get all the transactions involving a specific address (output as a string)
        public String GetBlockTransactions(String address)
        {
            String output = String.Empty;
            
            foreach (Block b in Blocks)
            {
                b.transactionList.ForEach(t =>
                {
                    if (t.senderAddress== address || t.receiverAddress==address) { output += (t.ToString() + "\n"); }
                });
            }

            return output;
        }

        // Merkle Root validation method
        public bool validateMerkleRoot(Block b)
        {
            String reMerkle = Block.MerkleRoot(b.transactionList);
            return reMerkle.Equals(b.merkleRoot);
        }
    }
}
