﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlockchainAssignment
{
    public partial class BlockchainApp : Form
    {
        Blockchain blockchain;
        public BlockchainApp()
        {
            // Creates the blockchain along with the Genesis Block
            InitializeComponent();
            blockchain = new Blockchain();
            richTextBox1.Text = "New Blockchain Initialized!";
        }



        private void PrintBlock_Click(object sender, EventArgs e)
        {
            // Used to call the components of the block based on index
            int index = 0;
            if(Int32.TryParse(textBox1.Text, out index))
            {
                richTextBox1.Text = blockchain.GetBlockAsString(index);
            }
        }

        // Generate Wallet button
        private void GenerateWallet_Click(object sender, EventArgs e)
        {
            // Initializes the private key
            String privKey;

            // Generates the wallet, including public and private keys, and
            // outputs to the textboxes the public and private keys respectively
            Wallet.Wallet myNewWallet = new Wallet.Wallet(out privKey);
            publicKey.Text = myNewWallet.publicID;
            privateKey.Text = privKey;
        }

        // Validating the Keys button
        private void ValidateKeys_Click(object sender, EventArgs e)
        {
            // Checks if the keys are related or not
            if(Wallet.Wallet.ValidatePrivateKey(privateKey.Text, publicKey.Text))
            {
                richTextBox1.Text = "Keys are Valid";
            }
            else
            {
                richTextBox1.Text = "Keys are Invalid";
            }
        }

        // Creates a transaction
        private void CreateTransaction_Click(object sender, EventArgs e)
        {
            bool validTransaction = true;
            String errorMessage = string.Empty;

            // Validation for presence of a public key
            if (publicKey.Text == "")
            {
                validTransaction = false;
                errorMessage = "Lack of sender address";
            }
            // Validation for presence of an amount to be sent
            else if (amount.Text == "")
            {
                validTransaction = false;
                errorMessage = "No amount specified";
            }
            // Validation for a fee to be assigned to transaction
            else if (fee.Text == "")
            {
                validTransaction = false;
                errorMessage = "No fee specified";
            }
            // Validation for private key fitting to the public key
            else if (!Wallet.Wallet.ValidatePrivateKey(privateKey.Text, publicKey.Text))
            {
                validTransaction = false;
                errorMessage = "Invalid Private Key";
            }
            // Validation method, checks the sender's balance is sufficient for the transaction
            else if (blockchain.GetBalance(publicKey.Text) < Convert.ToDouble(amount.Text))
            {
                validTransaction = false;
                errorMessage = "Insufficient Funds \n";
            }

            // Creates the transaction if valid, if not it reads out the error message and reason
            if (validTransaction)
            {
                Transaction transaction = new Transaction(publicKey.Text, receiverKey.Text, Double.Parse(amount.Text), Double.Parse(fee.Text), privateKey.Text);
                blockchain.transactionPool.Add(transaction);
                richTextBox1.Text = transaction.ToString();
            }
            else
            {
                richTextBox1.Text = "Invalid Transaction\n" + errorMessage;
            }
        }

        // Creates a new block via block constructor and adds it to the blockchain
        private void CreateBlock_Click(object sender, EventArgs e)
        {
            // Creates the new block based on pending transactions
            List<Transaction> transactions = blockchain.GetPendingTransactions();
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, publicKey.Text);
            blockchain.Blocks.Add(newBlock); // Adds to the blockchain

            // Prints to the screen the blockchain
            richTextBox1.Text = blockchain.ToString();
        }

        // To read all the blocks in the blockchain
        private void ReadAll_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = blockchain.ToString();
        }

        // Prints to screen the pending transactions
        private void ReadPendingTransactions_Click(object sender, EventArgs e)
        {
            String output = String.Empty;
            blockchain.transactionPool.ForEach(t => output += (t.ToString() + "\n"));
            richTextBox1.Text =  output;
        }

        private void ValidateChain_Click(object sender, EventArgs e)
        {
            // Contiguity Checks
            bool valid = true;

            // If only Genesis Block is there, it is 
            if (blockchain.Blocks.Count == 1)
            {
                if (!blockchain.validateMerkleRoot(blockchain.Blocks[0]))
                {
                    richTextBox1.Text = "Blockchain is invalid";
                }
                else
                {
                    richTextBox1.Text = "Blockchain is valid";
                }
                return;
            }
            for (int i = 1; i<blockchain.Blocks.Count - 1; i++)
            {
                // Checks that the order of the blocks (previous block hash is actually previous block hash) is in order.
                // Also checks transactions via merkle root.
                if (blockchain.Blocks[i].prevHash != blockchain.Blocks[i - 1].hash || !blockchain.validateMerkleRoot(blockchain.Blocks[i]))
                {
                    richTextBox1.Text = "Blockchain is invalid";
                    return;
                }
            }
            if (valid)
            {
                richTextBox1.Text = "Blockchain is valid";
            }
            else
            {
                richTextBox1.Text = "Blockchain is invalid";
            }
        }

        // Checks the balance and outputs transactions involving the wallet
        private void CheckBalance_Click(object sender, EventArgs e)
        {
            String output = String.Empty;
            output = "Address: " + publicKey.Text 
                + "\nBalance: " + blockchain.GetBalance(publicKey.Text).ToString() 
                + " Entropycoin\n"
                + "\n" + blockchain.GetBlockTransactions(publicKey.Text);

            richTextBox1.Text = output;
        }

        private void Greedy_Click(object sender, EventArgs e)
        {
            // Creates the new block based on transactions with the highest fees
            List<Transaction> transactions = blockchain.GetPendingTransactions("Greedy");

            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, publicKey.Text); // Creates a new block using lastblockhash, transactions, and miner address
            blockchain.Blocks.Add(newBlock); // Adds to the blockchain

            // Prints to the screen the blockchain
            richTextBox1.Text = blockchain.ToString();

        }

        private void AddressBased_Click(object sender, EventArgs e)
        {
            // Creates the new block based on sender addresses of transactions
            List<Transaction> transactions = blockchain.GetPendingTransactions("AddressBased", preferenceAddress.Text);
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, publicKey.Text); // Creates a new block using lastblockhash, transactions, and miner address
            blockchain.Blocks.Add(newBlock); // Adds to the blockchain

            // Prints to the screen the blockchain
            richTextBox1.Text = blockchain.ToString();
            
        }

        private void Altruistic_Click(object sender, EventArgs e)
        {
            // Creates the new block based using the oldest transactions in the transactionpool
            List<Transaction> transactions = blockchain.GetPendingTransactions("Altruistic");
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, publicKey.Text); // Creates a new block using lastblockhash, transactions, and miner address
            blockchain.Blocks.Add(newBlock); // Adds to the blockchain

            // Prints to the screen the blockchain
            richTextBox1.Text = blockchain.ToString();

            
        }

        private void Random_Click(object sender, EventArgs e)
        {
            // Creates the new block based on randomly chosen transactions 
            List<Transaction> transactions = blockchain.GetPendingTransactions("Random");
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, publicKey.Text); // Creates a new block using lastblockhash, transactions, and miner address
            blockchain.Blocks.Add(newBlock); // Adds to the blockchain

            // Prints to the screen the blockchain
            richTextBox1.Text = blockchain.ToString();
            
        }

    }
}
