﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TrackBar;

namespace BlockchainAssignment
{
    internal class Block
    {
       
        public int index; // Index
        DateTime timestamp; // Timestamp
        public String hash; // Hash of the Block
        public String prevHash; // Hash of the Previous Block

        
        public List<Transaction> transactionList = new List<Transaction>(); // List for transactions
        public String merkleRoot;

        // Proof-of-Work
        public long nonce = 0; // Nonce = 1-time use token for trust building
        private object _nonceLock = new object(); // Used to lock the nonce number being used by a thread to not repeat work
        public int threadsCount = 8; // Number of threads to be used
        public Stopwatch stopwatch; // The stopwatch to time the mining process
        public long elapsedTime; // Used to hold the time and for the read method
        public bool IsComplete; // Bool used to stop the other threads when the mining is complete
        public string finalHash; // The hash that fits the difficulty

        // Dynamic Difficulty
        public int difficulty = 4;
        public static int _difficulty = 4;
        public int expectedBlockTime = 10000; // in milli-seconds
        public double averageBlockTime = 0;
        public int blockLookback = 24;
        public static double totalTime = 0;

        // Rewards and fees
        public double reward = 1.0; // fixed logic
        public double fees = 0.0;

        public String minerAddress = String.Empty;


        // Genesis Block Constructor
        public Block()
        {
            timestamp = DateTime.Now;
            index = 0;
            prevHash = String.Empty;
            reward = 0;
            merkleRoot = MerkleRoot(transactionList);
            hash = Mine();
        }

        // Constructor for blocks by using two arguments
        public Block(int index, String prevHash)
        {
            this.timestamp= DateTime.Now;
            this.index = index + 1;
            this.prevHash = prevHash;
            this.hash = CreateHash(nonce);
        }

        // Constructor for blocks by using the previous block as argument
        public Block(Block lastBlock, List<Transaction> transactions, String address = "")
        {
            timestamp = DateTime.Now;
            index = lastBlock.index + 1;
            prevHash = lastBlock.hash;

            minerAddress = address;

            transactions.Add(CreateRewardTransaction(transactions));
            transactionList = transactions;

            // Adding Merkle Root algorithm
            merkleRoot = MerkleRoot(transactionList);

            // Adjusts the internal difficulty
            difficulty = _difficulty;
            
            hash = Mine();
        }

        // Calculate reward and total fees then creates a transaction of them
        public Transaction CreateRewardTransaction(List<Transaction> transactions)
        {
            // Sum the mined block's fees, in the list of transactions
            fees = transactions.Aggregate(0.0, (acc, t) => acc + t.fee);
            // Create the "Reward Transaction" being the sum of fees and reward transferred from "Mine Rewards" (Coin Base) to miner
            return new Transaction("Mine Rewards", minerAddress, (reward + fees), 0, "");
        }

        // Method to create the hash
        public String CreateHash(long nonce)
        {
            String hash = String.Empty;

            SHA256 hasher = SHA256Managed.Create();

            // Concatenate all Block properties for hashing
            String input = index.ToString() + timestamp.ToString() + prevHash + nonce.ToString() + reward.ToString() + merkleRoot;

            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));
            // Converts the Hash from a byte array to a string
            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash;
        }

        // Loops until the hash corresponds to the difficulty criteria
        public String Mine()
        {
            String hash = CreateHash(nonce); // Create the initial hash
            Thread[] threads = new Thread[threadsCount]; // Sets up the thread array with size of pre-set thread count
            IsComplete = false; // Sets the requirement of complete to false first

            // Difficulty criteria definition
            String re = new string('0', difficulty); // Create a "regex" string of N (difficulty, i.e. 4) 0's
 
            // Declare the stopwatch and start it
            stopwatch = Stopwatch.StartNew();

            // Re-Hash until difficulty criteria is met
            for (int i = 0; i < threadsCount; i++)
            {
                ThreadStart threadDelegate = new ThreadStart(() => MineThread(i, re, hash)); // Declares the thread delegate with the minethread method with all arguments included 
                threads[i] = new Thread(threadDelegate);// Creates the new thread and adds to thread array
                Console.WriteLine("Thread " + i + " initiated"); // Writes to console the thread initiated
                threads[i].Start();// starts the thread      
            }
            while (!IsComplete) { } // Spin wait so no other action can be taken
            stopwatch.Stop(); // Stops the stopwatch

            elapsedTime = stopwatch.ElapsedMilliseconds; // The time the block took to mine

            // Total time to be used for the dynamic difficulty adjustment
            totalTime += elapsedTime;

            // Calls the adjust difficulty every 10 blocks and resets the totalTime that counted the previous 10 blocks.
            if (index % 10 == 0 && index != 0) // Makes sure it's every 10 blocks and doesn't count for the genesis block
            {
                // Adjusts the difficulty based on the average of the last 10 blocks.
                averageBlockTime = totalTime / 10;
                if (averageBlockTime < 0.8 * expectedBlockTime) // increases difficulty if the time is less than 80% of the expected block time
                {
                    _difficulty += 1; 
                }
                else if (averageBlockTime > 1.2 * expectedBlockTime) // decreases difficulty if the time is greater than 120% of the expected block time
                {
                    _difficulty -= 1;
                }
                totalTime = 0;
            }

            Console.WriteLine("Hash " + finalHash + " found in " + elapsedTime + "ms"); // A check that returns the hash and time to the console
            return finalHash; //  Returns the hash
        }

        // Method for the PoW threads
        public void MineThread(int threadnumber, string re, string hash)
        {
            long threadNonce; // Declares a new nonce for the thread

            // As long as the hash doesn't fulfil the difficulty requirement, it continues to calculate
            while (!hash.StartsWith(re))
            {
                if (IsComplete) return; // Returns the thread if another thread already has finished
                lock (_nonceLock) // Locks the nonce so that no other thread can take this nonce
                {
                    threadNonce = nonce++;  // Sets this thread's nonce to the old nonce + 1
                }
                hash = CreateHash(threadNonce); // Creates the hash
                //Console.WriteLine("Thread " + threadnumber + " with nonce " + threadNonce + " and hash " + hash); // To track nonce and thread progress 
            }
            IsComplete = true; // Once found, sets the complete bool to true to end the other threads
            finalHash = hash; // Sets the found hash for the other threads not to change the hash variable holding the current hash
        }

        // Merkle Root algorithm method, combines all transactions within a block into a single hash.
        public static String MerkleRoot(List<Transaction> transactionList)
        {
            List<String> hashes = transactionList.Select(t => t.hash).ToList();
            if(hashes.Count == 0)
            {
                return String.Empty;
            }
            if(hashes.Count == 1)
            {
                return HashCode.HashTools.CombineHash(hashes[0], hashes[0]);
            }
            while(hashes.Count != 1)
            {
                List<String> merkleLeaves = new List<String>();
                for (int i=0; i<hashes.Count; i += 2)
                {
                    if(i == hashes.Count - 1)
                    {
                        merkleLeaves.Add(HashCode.HashTools.CombineHash(hashes[i], hashes[i]));
                    }
                    else
                    {
                        merkleLeaves.Add(HashCode.HashTools.CombineHash(hashes[i], hashes[i + 1]));
                    }
                }
                hashes = merkleLeaves;
            }
            return hashes[0];
        }
      

        // Method to view the called block's components.
        public override string ToString()
        {
            String output = String.Empty;
            transactionList.ForEach(t => output += (t.ToString() + "\n"));

            return "[BLOCK START]\n\n"
                + "--------Block " + index.ToString() + "----------\n"
                + "Index: " + index.ToString()
                + "     Timestamp: " + timestamp.ToString()
                + "\nPrevious Hash: " + prevHash
                + "\nHash: " + hash
                + "\nMerkle Root: " + merkleRoot
                + "\nNonce: " + nonce.ToString()
                + "\nDifficulty: " + difficulty.ToString()
                + "\nReward: " + reward.ToString()
                + "     Fees: " + fees.ToString()
                + "\nMiner's Address: " + minerAddress
                + "\nNumber of threads used: " + threadsCount.ToString()
                + "\nMined in: " + elapsedTime + "ms\n"
                + "\n--------Transactions----------"
                + "\nTransactions Count:" + transactionList.Count + "\n"
                + "\n" + output + "\n"
                + "[BLOCK END]\n";
        }
    }
}
